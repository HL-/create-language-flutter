## 安装
```
npm i create-language-flutter -g
```

## 无配置使用
#### 在需要生成字体包的目录执行命令，默认生成zh.json、en.json、untranslated.json
```
 clf create font
 ```
 该命令会找出所有$T("关键字")里的关键字

 ```
$T('你好')
$T('小明今年%s'，[age])
 ```
生成的zh.json
```
{
   n1:"你好",
   n2:"测试",
   n3:"小明今年 {age} 岁了",
   n4:"标题
 }
 ```
## 写入
#### 在untranslated.json里翻译好对应的en，将文件名改为translated.json，执行命令
```
 clf write en
 ```

## 生成国际化dart文件

、、、
  clf generate locales
、、、
## 配置
 在执行命令目录下创建一份配置文件create-languate.json

 ```
 {
   entry:"入口目录",//默认"."
   output:"字体包生成目录",//默认"."
   generate:"配置国际化文件的导出目录", // 默认"."
 }
 ```