

import path from "path"
import readFile from "./readFile.js";
export default async ()=>{
 
 
  let defaultOption = {
    language: ['zh_CN','en_US'], // 配置国际化语言类型
    mainLanguage: 'zh_CN', // 默认主语言
    entry : '.',
    output:'.',
    generate: '.',
  }
  let option = await readFile(path.join('file://',process.cwd(),'create-language.mjs'))
  if(option){
    return {...defaultOption,...option}
  }
 return defaultOption;
}