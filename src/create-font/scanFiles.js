import path from "path";
import fs from "fs";
// const ignore = ["common"];
const files = ["pages"];
export default function scanFile(root) {
  let arrFiles = [];
  const files = fs.readdirSync(root);
  for (let i = 0; i < files.length; i++) {
    const item = files[i];
    const stat = fs.lstatSync(path.join(root, item));
    if (stat.isDirectory() === true) {
      if (files.includes(item)) {
        arrFiles = arrFiles.concat(scanFile(path.join(root, item)));
      }
    } else {
      if (/^.*\.(dart)$/.test(item)) {
        /* 只提取dart文件 */
        arrFiles.push(path.join(root, item));
      }
    }
  }
  return arrFiles;
}