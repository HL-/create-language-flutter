import chalk from "chalk";
import ora from "ora";
import fs from "fs";
import path from "path";
import getConfigOption from "../common/getConfigOption.js";
import readFile from "../common/readFile.js";

const spinner = ora();

const ignore = ["untranslated.json", "translated.json"];

function createMap(zhFile) {
  const map = new Map();
  for (let key in zhFile) {
    map.set(zhFile[key], key);
  }
  return map;
}

async function writeGenerateFile(files, dir, fileName) {
  try {
    fs.statSync(dir);
  } catch (err) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(
    path.join(dir, fileName),

    await writeFileTemplate(files),
    { encoding: "utf8" }
  );
  return path.join(dir, fileName);
}

async function writeFileTemplate(files) {
  const tempalte = `abstract class Locales {
        ${await spliceTemplateLocale(files)}
    }
    `;
  return tempalte;
}

async function spliceTemplateLocale(files) {
  let result = "";
  let configOption = await getConfigOption();

  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (!ignore.includes(file)) {
      const name = file.split(".")[0];

      let data = fs.readFileSync(
        path.join(process.cwd(), configOption.output, file)
      );
      result =
        result +
        "\n" +
        `
      static Map<String, String> get ${name} {
      return ${data};
    }`;
    }
  }
  return result;
}

export default async () => {
  try {
    // 读取配置
    let configOption = await getConfigOption();

    spinner.start(chalk.cyan("开始读取intl文件夹"));

    // let files = scanFiles(path.join(process.cwd(),configOption.output))
    let files = fs.readdirSync(path.join(process.cwd(), configOption.output));

    if (files.length) {
      console.log(files);
    } else {
      console.log(
        `${configOption.output}文件夹下未检索到.json格式的国际化文件`
      );
    }
    spinner.succeed(chalk.green("读取intl文件夹完成.."));

    spinner.start(chalk.cyan("生成国际化文件..."));
    let filePath = await writeGenerateFile(
      files,
      path.join(process.cwd(), configOption.generate),
      "locales.dart"
    );
    spinner.succeed(chalk.green("生成国际化文件:", filePath));
  } catch (err) {
    spinner.fail();
    console.log(err);
  }
};
