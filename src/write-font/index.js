
import chalk from "chalk";
import ora from "ora";
import fs from "fs"
import path from "path";
import getConfigOption from "../common/getConfigOption.js"
import readFile from "../common/readFile.js";
const spinner = ora();

function createMap(zhFile){
  const map = new Map();
  for(let key of Object.keys(zhFile)){
    map.set(zhFile[key],key)
  }
  return map
}
function writeFontFile(keywords,dir,fileName) {
 
  try {
    fs.statSync(dir);
  } catch (err) {
    fs.mkdirSync(dir);
  }

  fs.writeFileSync(
    path.join(dir,fileName),
    writeFileTemplate(JSON.stringify(keywords, null, 2)),
    { encoding: "utf8" }
  );
  return path.join(dir, fileName);
}


function writeFileTemplate(data) {
  const tempalte = `${data}`;
  return tempalte;
}

function objToStrMap(obj) {
  let strMap = new Map();
  for (let i of Object.keys(obj)) {
    strMap.set(i,obj[i]);
  }
  return strMap;
}

export default async () => {
  try {
    // 读取配置
    let configOption = await getConfigOption();


    // 读取已有文件包
    let zhFile = fs.readFileSync(path.join(process.cwd(),configOption.output,'zh.json'));
    let enFile = fs.readFileSync(path.join(process.cwd(),configOption.output,'en.json'));
    let translated = fs.readFileSync(path.join(process.cwd(),configOption.output,'translated.json'));


    let zhMap = createMap(JSON.parse(zhFile));
    var enMap = objToStrMap(JSON.parse(enFile));
    let translatedMap = JSON.parse(translated);

    if(!zhFile || !enFile || !translated){
      console.log("缺少zh.json或en.json或translated.json")
      return false
    }
    spinner.start(chalk.cyan("正在对比字体包..."));
    
    for(let k in translatedMap){
      console.log(k);
      let value = translatedMap[k].zh;
      let key = zhMap.get(value);
      if(key && translatedMap[k].en){
        enMap[key] = translatedMap[k].en
      }
    }
    
    // enJson = JSON.stringify(enMap);
    console.log(enMap);
    spinner.succeed(chalk.green("字体包对比完成"));
    spinner.start(chalk.cyan("写入文件..."));
    let filePath = writeFontFile(enMap,path.join(process.cwd(),configOption.output,),'en.json');
    spinner.succeed(chalk.green("写入文件:", filePath));
   
  } catch (err) {
    spinner.fail();
    console.log(err);
  }
};